<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/test',  function (Request $request) {
//
//    $http = new GuzzleHttp\Client;
//
//    $response = $http->post('http://api-github.test/oauth/token', [
//        'form_params' => [
//            'grant_type' => 'password',
//            'client_id' => 7,
//            'client_secret' => '6sm7yPzZSDPbOhNVA5XC1NouwsG1Vf0WQNFyNnMz',
//            'username' => 'test@test.con',
//            'password' => '123123',
//            'scope' => '',
//        ],
//    ]);
//
//    return json_decode((string) $response->getBody(), true);
//});


//Route::get('show', function (){
//
//
//    $response = $http->post('http://api-github.test/oauth/token', [
//        'form_params' => [
//            'email' => 'test@test.con',
//            'password' => '123123',
//        ],
//    ]);
//});


//Route::get('/redirect', function () {
//    $query = http_build_query([
//        'client_id' => 4,
//        'redirect_uri' => 'http://api-github.test/callback',
//        'response_type' => 'code',
//        'scope' => ''
//    ]);
//
//    return redirect('http://api-github.test/oauth/authorize?'.$query);
//});

//Route::get('callback', function (Request $request) {
//    $http = new GuzzleHttp\Client;
//
//    $response = $http->post('http://api-github.test/oauth/token', [
//        'form_params' => [
//            'grant_type' => 'authorization_code',
//            'client_id' => 4,
//            'client_secret' => 'UbuYWTlScpTmBpczlN9nbxa3V1FKMJUkqcILfkct',
//            'redirect_uri' => 'http://api-github.test/callback',
//            'code' => $request->code,
//        ],
//    ]);
//
//    return json_decode((string) $response->getBody(), true);
//});

//Route::get('callback/{name}', function ($name) {
//
//    $http = new GuzzleHttp\Client;
//    $response = $http->get('https://api.github.com/users/'.$name);
//    dump(json_decode($response->getBody()));
//});


Route::get('curl', 'HomeController@curl');

//Route::get('/redirect', function () {
//    $query = http_build_query([
//        'client_id' => '4',
//        'redirect_uri' => 'http://api-github.test/callback_uri',
//        'response_type' => 'code',
//        'scope' => '',
//    ]);
//
//    return redirect('/oauth/authorize?'.$query);
//});
//
//
//Route::get('callback_uri', 'HomeController@callback_uri');