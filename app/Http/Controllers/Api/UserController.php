<?php

namespace App\Http\Controllers\Api;

use App\Mail\OrderShipped;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $success = 200;

    /**
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request, User $user)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = $user->create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        $success['avatar'] = $user->avatar;
        return response()->json(['success' => $success], $this->success);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {

        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $success['access_token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->success);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function show()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->success);
    }

    /**
     * @param Request $request
     */
    public function github(Request $request)
    {

        $user = $this->apiRequest('https://api.github.com/users/'.$request->input('name'));

        if (!empty($user['email'])) {
            Mail::to($user['email'])->send(new OrderShipped($request->input('msg')));
        }

        return response()->json(['success' => $user], $this->success);
    }


    public function apiRequest($url)
    {
        $ch = curl_init();

        // Basic Authentication with token
        // https://developer.github.com/v3/auth/
        // https://github.com/blog/1509-personal-api-tokens
        // https://github.com/settings/tokens
       // $access = 'username:token';

        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Agent smith');
        curl_setopt($ch, CURLOPT_HEADER, 0);
      //  curl_setopt($ch, CURLOPT_USERPWD, $access);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        $result = json_decode(trim($output), true);
        return $result;
    }
}
