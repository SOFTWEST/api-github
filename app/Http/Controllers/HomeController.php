<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }


    public function curl()
    {
        $crl = curl_init('http://api-github.test/api/show');

        $headr = array();
        $headr[] = 'Content-length: 0';
        $headr[] = 'Cache-Control: no-cache';
        $headr[] = 'Content-type: application/json';
        $headr[] = 'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU1ZTg1NjExNmE4ZmM4NDgwNGU3N2RlZGVmNTdlOWMyYmE0ZTQ2OTM4NDBkZGRiNzA4YmM1MjdiMDAwMTM5YzNhYWQwNzZlNWFlNzBlYjdhIn0.eyJhdWQiOiI3IiwianRpIjoiNTVlODU2MTE2YThmYzg0ODA0ZTc3ZGVkZWY1N2U5YzJiYTRlNDY5Mzg0MGRkZGI3MDhiYzUyN2IwMDAxMzljM2FhZDA3NmU1YWU3MGViN2EiLCJpYXQiOjE1MzMwNDE4NTMsIm5iZiI6MTUzMzA0MTg1MywiZXhwIjoxNTY0NTc3ODUzLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.XruDsavqAbF4xCwk4anUksP-GlI1cBP_baNx0D6Im7NtJjdILqWVm9tVbRrTrXgRSA5Zx4FqJglzEekIWv1M22ShElo9OfFtUt3z3mPJgUq2MU4OSGTta4CY4ux57g34lQeMbMJyDfB1yuhceUPm3gp6H4dh_8dmm0gCL_o1duTOh4Mu7HgoUgmkR9wsP-4Ms_Frx0Q1kePeCbwxw2eUMtQbhXrNkSfRe3JKR_ZUj5zQxAfcpWP6JXnFLye2Ra0KZxmZZ2jhV6YoXnU9cGWZILyZZGwkcVSFHMZxz27nS2WLORu9D41zVsj404JtEwvXeEoj7FZd_Eg8LFTv2jhSOyWlLIg6UHeI_LelGNpPosXSbbHOM0PSd2egqVoZ5-TuJLo28MsApU0w9kv0xBwFgM-TFMHLH3tynmnpWSolcgLKVoZlF0qY8YbF2xEttl2xYbwXh2_FzK4xYXIXcqlwBDfwogaV7qx8sCDXK5beck0ijwFACZrJ9VAyqIwNp-8bE8lUwlCKRQxSurH46boRf1Khse5QkYkwFGqWAL9SOUQxbj6F9TFPkSFtyeho9ou0GVLL4qNsLZt2SJ-kgy4-nvIO33zIX5GQJn-5IAyFvnIBOAWo1A4jz4XMBEey-kK8M2vM45d1r89h5-6c8RiWXRHFQgtL_4q4eNFsjgtmYT8';

        curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
        curl_setopt($crl, CURLOPT_POST,true);
        $rest = curl_exec($crl);

        curl_close($crl);

    }
    
}
